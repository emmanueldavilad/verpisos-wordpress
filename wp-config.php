<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'verpisos' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'N-bM5]2)4G`MFJI|$[8(!00d`opy.xx[PC8my6U;^yM~i2:]-Csuycn`oqQV}QtD' );
define( 'SECURE_AUTH_KEY',  '%{=},@qAS;vz7kik<+aY(iF#E(.: ||L=-NMjcV;[Jk~Fxr4~AKNz5[[iZ]L?&L]' );
define( 'LOGGED_IN_KEY',    'Cz&RZ5ta{:2Y(i }yeuaUD@8W-}m=J.$cZ%ua/:Uo9m=y2<A9{w/zr-Mgw3-V~X<' );
define( 'NONCE_KEY',        '_Q7p#)d%E&+J}lt85,zGnDs/k8N%hm90-}<a6 P=4_c6]E9jTOc`ByH9eC`[nq?8' );
define( 'AUTH_SALT',        '.3Oft_F&}Q5>-8JSeI.:%CkZSfoJQJqj*W=&Qh}T:k ([!2R~dQm-X[Xm,nF.dte' );
define( 'SECURE_AUTH_SALT', 'j-{Z>l^49g#hDV^o2]- 6s}W#NW!c?D9sV&%@_Mk|b!G9t_3Kvy.ehe8;euy3MB/' );
define( 'LOGGED_IN_SALT',   't#,G;n}i^$~mT~try>YQY[c!w?ghe|O$g#QO(o`8b^p24sw-c,&uK#p{K>WNyv)q' );
define( 'NONCE_SALT',       'Zs[eJN;~KDvKGO/G?E32wfTb-fXEUM/mki*BBo+,dPa_L),YcnA@3;I_cH5ug|FD' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
